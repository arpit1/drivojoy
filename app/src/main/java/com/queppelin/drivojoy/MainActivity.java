package com.queppelin.drivojoy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setHeader("Home");

        LinearLayout bike_detail_row = (LinearLayout) findViewById(R.id.bike_detail_row);
        bike_detail_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ChoosePlanActivity.class);
                startActivity(intent);
            }
        });

    }
}
