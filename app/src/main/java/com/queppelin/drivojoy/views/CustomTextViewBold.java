package com.queppelin.drivojoy.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatTextView;


/**
 * Created by arpit on 25-Nov-15.
 */
public class CustomTextViewBold extends AppCompatTextView {
    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Semibold.ttf");
        this.setTypeface(face);
    }
}
