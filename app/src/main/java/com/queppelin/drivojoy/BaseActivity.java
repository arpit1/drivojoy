package com.queppelin.drivojoy;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by arpit on 3/21/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public void setHeader(String name){
        TextView header = (TextView) findViewById(R.id.header);
        header.setText(name);

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
