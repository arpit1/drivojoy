package com.queppelin.drivojoy;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    private Handler splashTimeHandler;
    private Runnable finalizer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        splashTimeHandler = new Handler();
        finalizer = new Runnable() {
            public void run() {
//                if (getFromPrefs(WudStayConstants.EMAIL).equals("")) {
//                    Intent mainIntent = new Intent(ctx, LoginActivity.class);
//                    startActivity(mainIntent);
//                } else {
                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(mainIntent);
//                }
                finish();
            }
        };
        splashTimeHandler.postDelayed(finalizer, 2000);
    }
}
