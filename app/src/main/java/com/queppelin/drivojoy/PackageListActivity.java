package com.queppelin.drivojoy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.queppelin.drivojoy.adapter.PackageListdapter;

public class PackageListActivity extends BaseActivity implements View.OnClickListener{

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private PackageListActivity ctx=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_list_package);

        setHeader("Select Packages");

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);

        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new PackageListdapter(this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        String name = (String) view.getTag(R.string.key);
        String ServiceType = (String) view.getTag(R.string.data);
        String Validity = (String) view.getTag(R.string.val);
        String Amount = (String) view.getTag(R.string.amount);
        System.out.println("hh name is "+name);
        System.out.println("hh ServiceType is "+ServiceType);
        System.out.println("hh Validity is "+Validity);
        System.out.println("hh Amount is "+Amount);

        Intent intent = new Intent(ctx, PackageDetailActivity.class);
        intent.putExtra("name", name);
        intent.putExtra("Service", ServiceType);
        intent.putExtra("Validity", Validity);
        intent.putExtra("Amount", Amount);
        startActivity(intent);

    }
}
