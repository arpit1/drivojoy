package com.queppelin.drivojoy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class IndividualPackageActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_package);

        setHeader("Select Services");

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);

        LinearLayout next = (LinearLayout) findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IndividualPackageActivity.this, ScheduleVisit.class);
                startActivity(intent);
            }
        });
    }
}
