package com.queppelin.drivojoy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ChoosePlanActivity extends BaseActivity {

    private LinearLayout individual, packages;
    private ChoosePlanActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_plan);

        setHeader("Choose Plan");

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);

        individual = (LinearLayout)findViewById(R.id.individual);
        packages = (LinearLayout)findViewById(R.id.packages);

        individual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChoosePlanActivity.this, IndividualPackageActivity.class);
                startActivity(intent);
            }
        });

        packages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, PackageListActivity.class);
                startActivity(intent);
            }
        });
    }
}
