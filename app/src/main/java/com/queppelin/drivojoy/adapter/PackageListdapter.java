package com.queppelin.drivojoy.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.queppelin.drivojoy.PackageListActivity;
import com.queppelin.drivojoy.R;
import com.queppelin.drivojoy.pojo.PackagePojo;

import java.util.ArrayList;

public class PackageListdapter extends RecyclerView.Adapter<PackageListdapter.ViewHolder> {

    ArrayList<PackagePojo> mItems;

public PackageListActivity ctx;


    public PackageListdapter(PackageListActivity ctxx) {
        super();

        ctx=ctxx;
        mItems = new ArrayList<PackagePojo>();


        PackagePojo appoint3 = new PackagePojo();
        appoint3.setName("Drivo Care+");
        appoint3.setServiceType("12 Full Services");
        appoint3.setValidity("24 Months Validity");
        appoint3.setPrice(ctx.getResources().getString(R.string.Rs)+" 2200");
        mItems.add(appoint3);

        PackagePojo appoint5 = new PackagePojo();
        appoint5.setName("Drivo Care");
        appoint5.setServiceType("9 Full Services");
        appoint5.setValidity("18 Months Validity");
        appoint5.setPrice(ctx.getResources().getString(R.string.Rs)+" 2000");
        mItems.add(appoint5);

        PackagePojo appoint2 = new PackagePojo();
        appoint2.setName("Overhaul Tune Up");
        appoint2.setServiceType("6 Full Services");
        appoint2.setValidity("16 Months Validity");
        appoint2.setPrice(ctx.getResources().getString(R.string.Rs)+" 1600");
        mItems.add(appoint2);

        PackagePojo appoint4 = new PackagePojo();
        appoint4.setName("Major Tune Up");
        appoint4.setServiceType("5 Full Services");
        appoint4.setValidity("14 Months Validity");
        appoint4.setPrice(ctx.getResources().getString(R.string.Rs)+" 1200");
        mItems.add(appoint4);

        PackagePojo appoint = new PackagePojo();
        appoint.setName("Basic Tune Up");
        appoint.setServiceType("3 Full Services");
        appoint.setValidity("12 Months Validity");
        appoint.setPrice(ctx.getResources().getString(R.string.Rs)+" 1000");
        mItems.add(appoint);




    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.package_list_adapter, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        PackagePojo appoint = mItems.get(i);
        viewHolder.name.setText(appoint.getName());
        viewHolder.services_provide.setText(appoint.getServiceType());
        viewHolder.validity.setText(appoint.getValidity());
        viewHolder.price.setText(appoint.getPrice());

        viewHolder.subscribe.setTag(R.string.key, viewHolder.name.getText().toString());
        viewHolder.subscribe.setTag(R.string.data, viewHolder.services_provide.getText().toString());
        viewHolder.subscribe.setTag(R.string.val, viewHolder.validity.getText().toString());
        viewHolder.subscribe.setTag(R.string.amount, viewHolder.price.getText().toString());
        viewHolder.subscribe.setOnClickListener(ctx);


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView name, services_provide, validity, price;
        public LinearLayout subscribe;
        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.name);
            services_provide = (TextView)itemView.findViewById(R.id.services_provide);
            validity = (TextView)itemView.findViewById(R.id.validity);
            price = (TextView)itemView.findViewById(R.id.price);
            subscribe = (LinearLayout)itemView.findViewById(R.id.subscribe);

        }
    }
}

